## Bio Introduction


**Alberto Galan** is an **expert in Digital Business**, through busines data analysis can generate Marketing Growth, Market Segmentation and Accelerate Innovation. He has dilated experience in **Data Processing, Communication and Persuasion Techniques**, Neuro Linguistic Programming. **NLP technique** is an USA famous methodology for Efficiency Communication and Couching, that permits improve the relationship with customers and achieve faster personal goals. 


He has more than **20 years of experience** in different industries as **Biotechnology** ( Switzerland, Biorad ), **Automated Storage and Retrieval Systems** ( Switzerland, Kardex ), **High Voltage Power Systems** ( China, Grandtop ), received **education from Spanish and UK Universities** and working with Spanish, German, Switzerland and Norway Enterpreneurs. Also contribute in Hacker and Colearning spaces: **Coderbunker (2015)** Linux Colearning Leader, teaching advanced computer skills. **Biohacker Barcelona (2014)** organizing and collaborating to create laboratory machines.


Alberto also is an experimental artist and creator: *Human Power Limits (2013)*, through motivation techniques crossing more than 800 km from Barcelona to Toledo with a simple folding bike and a DIY cooking solar system. *Solar Heat Wall System (2012)*. Alberto has spent more than 10 years with DIY online communities and open source projects, this valuable mindset could couch emplooyes to improve themselves. 


<p align="center">
"Imagination is a way of living. Now live your own dream" 
</p>
