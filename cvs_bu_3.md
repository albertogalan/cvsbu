Alberto Galan CV
-----------------

<img src="https://i.imgur.com/AFrDopd.png" width="140" height="162" /> <img src="https://i.imgur.com/FVQcQHr.png" width="140" height="162" />

https://github.com/albertogalan
Phone : +8618721846147
E- mail : <info@albertogalan.com>

## **Digital Marketing**

**Business Fashion cooperation 2018**

- Wuhan International Fashion Center (Fosun Group) - Physical and Digital Space for Chinese designers: 
  - Initial cooperation to attract foreign brands into the new installations

  - Physical and Digital platform for Chinese fashion designers to do B2B

  - Total investment of this project 2000 mEuros

**Business Developer - [www.grandtop.cn](http://www.grandtop.cn/) 2017 - May 2019, Shanghai**

-   Automate Lead generation through scrapping 

-   Business mediator between Spanish Customer and Chinese supplier

-   Installation and configuration Open ERP to manage leads website [wwww.tornae.com](http://www.tornae.com)

-   Leads offers
    -  Found 1 project of around 2 mEuros in Madrid, the biggest Electric laboratory in Spain
    -  Found 2 projects for metalworking in Spain 

**Marketing Manager - [www.prittypigments.com](http://www.prittypigments.com/) - Sep 2015 – Jan 2017, Shanghai**

-   Persuading to invest in a Financial Database to retrieve key data

-   Providing key customers around the world around 1000 from a Global Financial Data Provider – Give key information in a small amount of time.

-   Website building and maintaining, SEO analysis

**Marketing Manager - [www.etsystems.com](http://www.etsystems.com/) – Jan 2010 – Jan 2015, Barcelona**

-   Through Customer Market research and Persuade Techniques I created following projects:

    -   Textile Retail: www.zara.com : 2-3 mEuros

    -   Textile Retail: [www.desigual.com](http://www.desigual.com/) : 2 mEuros

    -   Textile Retail: [www.pronovias.com](http://www.pronovias.com/) : 300,000 Euros

    -   Luxury garments Retail: [www.tous.cn](http://www.tous.cn/) : 400,000 Euros

    -   Food Retail: [www.mercadona.com](http://www.mercadona.com/) : 2-3 mEuros

    -   Luxury Watches Retail: [www.festinagroup.com](http://www.festinagroup.com/) : 1 mEuros

    -   Professional Hairdress: [www.salerm.com](http://www.salerm.com/) : 400,000 Euros

    -   Perfumes Retail: Unidroco, Julia,

    -   Cosmetics/Laboratories: Novartis

-   Google SEO, SEM, Marketing campaigns -- SEO was 1<sup>st</sup> position during years

-   2<sup>nd</sup> in volume sales in Euro area 6mEuros for Megamat brand

-   3 years of growing visitors

**Business Consultant – [www.kardex.com](http://www.kardex.com/) Feb 2003 – Feb 2006, Barcelona**

-   Sales manager in intra-logistic automation -

-   Negotiating and deals

**Engineer Maintenance** – [www.diamed.com](http://www.diamed.com/) - Jun 1998 - Sep 2003, Madrid


- Repairing, teaching and reporting clinic diagnose electro-medicine machines. My interlocutors were doctors and nurses.

### **Digital Project experiences**

**System Linux Administrator and Devops – Ansible – Jun – Now 2018 – Shanghai**

-   Monitor system maintenance SNMP in CentOs – 12h/5day service

**Automate Trading Digital Coin Investment – Mar - Jun 2018 - Shanghai**

-   Persuading Digital Coin investors to get profit from automation

-   Negotiating seed investment worth 500,000 RMB

**Translation Web App** – Node.js, JS and Python – Jan – Mar 2018 

-   Back and Front End

-   Multiple functionalities to learn and understand Chinese documents as OCR and automatic translation

**Devops Mining Rig construction – Ansible - Dec 2017 - Shanghai**

-   Deploy a 6 AMD GPUs through Ansible automation tools – calculated expecting ROI 200%

**Market Research Import/Export – JScript - Oct 2017 - Shanghai**

-   Scraping information public tender database – reduce the amount of time 1/1000 to get the key companies are working with metal products

**Devops for DLG Digital Luxury Agency – Ansible – Jul -Aug 2017 - Shanghai**

-   Automated Deploying Digital Infrastructure for **digitalsociety.com –** avoid errors and reduce amount of time to maintain servers 1/50

**Devops for eiceducational.com - Ansible - Dec 2016 - Shanghai**

-   Deploying and maintaining Server Infrastructure – avoid errors and reduce amount of time to maintain server 1/50

**Data analysis – JScript – 2017 – Shanghai**

-   Financial data, Scraping information around 1 million rows – analyze which are the customers, market research analysis

-   Import Export Data, Scraping information – market research analysis

**Data analysis – R – 2013/2014 – Barcelona**

-   Factorial Analysis with Jupyter– Customer Market Research

## **EDUCATION**

-   **Fudan University** Chinese Language I, Chinese Language, 2015 – 2016

-   **Neurolinguistic programming (NLP)** : Practicioner - 2013

-   **BA (Honor's) Politics and Economics OU University** – London – 2010 – 2015

-   **Market Research Master, Marketing UOC University** – Barcelona - 2006 - 2010

-   **Electronics Engineer Bachelor’s UAH University** – Madrid - 1993 – 1997

    -   1<sup>st</sup> obtaining degree within 120 people, I spent 1 year less than the rest of students.

## ***LANGUAGES:*** 
 
English, Chinese, Spanish, Catalan

## **COLLABORATION**

1<sup>st</sup> member in Coderbunker.com . Coderbunker is first software developer community in Shanghai
