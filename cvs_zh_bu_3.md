Alberto Galan CV
----------------

<img src="https://i.imgur.com/AFrDopd.png" width="140" height="162" /> <img src="https://i.imgur.com/FVQcQHr.png" width="140" height="162" />

电话 : +86 18721846147

电子邮件 : [*info@albertogalan.com*](mailto:info@albertogalan.com)

## **业务拓展与市场营销经验**

**商业开发者- www.grandtop.cn 从2017到现在，上海**

-   准备与中方合作创建西班牙公司

-   欧美主导研究-发现约50家公司/实验室

-   西班牙客户与中国供应商之间的商务调解人

-   在西班牙最大的电气实验室马德里，发现了1个项目，大约2 Meurs

-   国际铅生成展

**业务开发者- www.tornae.com - 从2018到现在，上海**

-   创建自己的企业。

-   作为中国供应商和西班牙买家的采购代理的国际业务发展——找到一个客户

-   开erp实现(odoo)

-   自动领先一代从linkedin.com到odoo

**营销经理- [www.prittypigments.com](http://www.prittypigments.com/) – 从2016六月到 2017一月，上海**

-   说服投资金融数据库检索关键数据

-   从全球金融数据提供商在全球范围内提供1000个关键客户——在短时间内提供关键信息。

-   网站建设与维护、SEO分析

-   从头创建内容管理系统

#### **市场部经理 - [*www.etsystems.com*](http://www.etsystems.com/) – 从2010一月到 2015一月，巴塞罗那**

-   通过客户市场调研和说服技术，我创造了以下项目：

    -   纺织品零售: www.zara.com : 200-300 万欧元

    -   纺织品零售: [www.desigual.com](http://www.desigual.com/) : 200 万欧元

    -   纺织品零售: [www.pronovias.com](http://www.pronovias.com/) : 30 万欧元

    -   奢侈品服装零售: [www.tous.cn](http://www.tous.cn/) : 40 万欧元

    -   食品零售: [www.mercadona.com](http://www.mercadona.com/) : 200-300 万欧元

    -   高档手表零售: [www.festinagroup.com](http://www.festinagroup.com/) : 100 万欧元

    -   专业美发师: [www.salerm.com](http://www.salerm.com/) : 40 万欧元

    -   香水零售: Unidroco, Julia,

    -   化妆品/实验室: Novartis

-   谷歌搜索引擎优化，SEM，营销活动-- SEO是第一年的位置

-   欧洲第二卷销售量. 600 万欧元 （西班牙市场）一年内品牌超过100台

-   3年游客不断增加

**商务顾问 – [www.kardex.com](http://www.kardex.com/) 从2月2000年到2006二月在巴塞罗那**

-   物流内部自动化销售经理

-   谈判与交易

#### 维修工程师– [*www.diamed.com*](http://www.diamed.com/) - 从1998六月 - 2003九月在马德里

-   修复、教学和报告临床诊断电工机器。我的对话者是医生和护士。

## **数字工程经验**

**自动交易数字硬币投资-马-军2018三月到六月 -上海**

-   说服数字钱币投资者从自动化中获利

-   谈判种子价值50 万元人民币

**采矿钻机建设-十二月2017 -上海**

-   通过自动化工具部署6 AMD GPU——计算预期ROI 200%

**市场调查进口/出口- 2017十月 -上海**

-   废除信息公开招标数据库 减少1000分之一的时间让关键公司使用金属产品

**Devops为DLG数字豪华代理-月-奥格2017 -上海**

-   为digitalsociety.com自动部署数字基础设施——避免错误并减少维护服务器1/50的时间

Devops 的**eiceducation.com** - 2016 六月到七月 - 上海

-   部署和维护服务器基础结构——避免错误和减少维护服务器1/50的时间

教育

-   汉语 复旦大学，上海 从2015到2016

-   市场研究硕士，营销UOC大学-巴塞罗那 从2006到2010

-   电子工程师学士UAH大学- 从1993到1997

    -   第一获得学位在120人以内，我花了1年时间比其余学生少。

-   神经语言学程序设计（NLP）：实践者 2013

    -   说服和快速学习的工具

**语言**：西班牙语、英语、加泰罗尼亚语、汉语

**协作**

第一名成员。Coderbunker是上海第一个软件开发者社区

